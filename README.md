
# Time Tracking App

# Steps to be followed to create React Project

1. Break the app into components.
2. Build a static version of the app
3. Determine what should be stateful
4. Determine in which component each piece of state should live
5. Hard code initial state
6. Add inverse data flow
7. Add Server communication


### For generate uuid:
- First install package named uuid:
command:
npm i uuid

- Then import it for use:
import {v4 as uuidv4} from 'uuid';

-usage:
id: uuidv4()





