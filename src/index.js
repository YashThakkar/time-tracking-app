import ReactDOM from 'react-dom';
import './style.css';
import TimerDashboard from './components/TimerDashboard';

ReactDOM.render(<TimerDashboard />, document.getElementById('root'));
